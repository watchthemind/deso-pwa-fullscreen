-keep class androidx.core.app.CoreComponentFactory { <init>(); }
-keep class androidx.core.content.FileProvider { <init>(); }
-keep class app.app_4everland.deso_frontend.twa.Application { <init>(); }
-keep class app.app_4everland.deso_frontend.twa.DelegationService { <init>(); }
-keep class app.app_4everland.deso_frontend.twa.LauncherActivity { <init>(); }
-keep class com.google.androidbrowserhelper.trusted.FocusActivity { <init>(); }
-keep class com.google.androidbrowserhelper.trusted.ManageDataLauncherActivity { <init>(); }
-keep class com.google.androidbrowserhelper.trusted.WebViewFallbackActivity { <init>(); }
-keep class androidx.browser.browseractions.BrowserActionsFallbackMenuView { <init>(android.content.Context, android.util.AttributeSet); }

